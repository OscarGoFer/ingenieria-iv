<!DOCTYPE html>
<html lang="es">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="reset.css">
     <link rel="stylesheet" href="style.css">
     <title>API DOG</title>
     <?php
     if (isset($_GET['enviar'])) {
          $url = 'https://dog.ceo/api/breeds/image/random';
          $jason = file_get_contents($url);
          $dato = json_decode($jason, true);
     ?>
          <style>
               div.image-dog {
                    background-image: url(<?php echo $dato['message'] ?>);
               }
          </style>
     <?php
     }
     ?>
</head>

<body>
     <h1 class="title">URL API: https://dog.ceo/api/breeds/image/random</h1>
     <div class="image-dog"></div>
     <form action="" method="get">
          <button type="submit" name="enviar">Ver otra imagen</button>
     </form>
</body>

</html>