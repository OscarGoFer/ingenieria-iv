<!DOCTYPE html>
<html lang="es">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio#3</title>
</head>

<body>
     <?php
     $conexion = pg_connect("host=localhost dbname=ejercicio1 user=postgres password=5022");
     $query = pg_query(
          $conexion,
          "         select
                         productos.nombre,
                         productos.precio,
                         marcas.nombre,
                         empresas.nombre,
                         categorias.nombre
                    from
                         productos
                    inner join
                         marcas
                    on
                         productos.id_marca = marcas.id_marca
                    inner join
                         empresas
                    on
                         marcas.id_empresa = empresas.id_empresa
                    inner join
                         categorias
                    on
                         productos.id_categoria = categorias.id_categoria"
     );
     echo "<table border align = 'center'>
                    <caption> TABLA DE PRODUCTOS </caption>";
     while ($row = pg_fetch_row($query)) {
          echo "
               <tr>
                    <td>$row[0]</td>
                    <td>$row[1]</td>
                    <td>$row[2]</td>
                    <td>$row[3]</td>
                    <td>$row[4]</td>
               </tr>";
     }
     echo "</table>";
     ?>
</body>

</html>