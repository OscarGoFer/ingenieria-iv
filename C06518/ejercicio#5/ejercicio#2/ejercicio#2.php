<?php

require 'vendor/autoload.php';
require 'conexion.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

/* selección de documento excel */

$direccionExcel = 'table.xlsx';
$excel = IOFactory::load($direccionExcel);

/* selección de hoja de planilla excel */
$hoja = $excel->getSheet(0);

/* obtención del número de filas con valores dentro de la hoja */
$numeroFilas = $hoja->getHighestDataRow();

/* obtención del número de columnas con valores dentro de la hoja */
$numeroColumnas = Coordinate::columnIndexFromString($hoja->getHighestDataColumn());

for ($fila = 2; $fila <= $numeroFilas; $fila++) {
     $sql = "INSERT INTO alumnos (nombre, apellido, cedula, matricula, carrera, nacionalidad) VALUES (";
     for ($columna = 1; $columna <= $numeroColumnas; $columna++) {
          $valor = $hoja->getCellByColumnAndRow($columna, $fila);
          if ($columna < $numeroColumnas) {
               $sql .= "'$valor',";
          } elseif ($columna == $numeroColumnas) {
               $sql .= "'$valor')";
          }
     }
     $resultado = $conexion->prepare($sql);
     $resultado->execute();
}
$resultado->closeCursor();
