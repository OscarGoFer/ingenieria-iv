<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 2</title>
</head>
<body>
     <?php
     echo "versión de php: " . phpversion() . "<br>";
     echo "ID de la versión php: " . PHP_VERSION_ID . "<br>";
     echo "Tamaño máximo admitido para enteros: " . PHP_INT_MAX . "<br>";
     echo "Tamaño máximo de nombre de un archivo: " . PHP_MAXPATHLEN . "<br>";
     echo "Versión del sistema operativo: " . php_uname() . "<br>";
     echo 'Símbolo correcto de Fin De Línea para la plataforma en uso: ' . PHP_EOL . '<br>';
     echo 'Include path por defecto: ' . DEFAULT_INCLUDE_PATH . '<br>';
     ?>
</body>
</html>