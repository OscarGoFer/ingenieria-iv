<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 4</title>
</head>
<body>
     <?php
     mt_srand();
     $var1 = mt_rand(50, 900);
     echo $var1 . '<br>';
     $var2 = mt_rand(50, 900);
     echo $var2 . '<br>';
     $var3 = mt_rand(50, 900);
     echo $var3 . '<br>';
     echo '<br>';
     if ($var1 >= $var2 && $var1 >= $var3) {
          echo "<span style='color:green;'>$var1</span> ";
          if ($var2 >= $var3) {
               echo "$var2 <span style='color:red;'>$var3</span>";
          } else {
               echo "$var3 <span style='color:red;'>$var2</span>";
          }
     } elseif ($var2 >= $var1 && $var2 >= $var3) {
          echo "<span style='color:green;'>$var2</span> ";
          if ($var1 >= $var3) {
               echo "$var1 <span style='color:red;'>$var3</span>";
          } else {
               echo "$var3 <span style='color:red;'>$var1</span>";
          }
     } elseif ($var3 >= $var1 && $var3 >= $var2) {
          echo "<span style='color:green;'>$var3</span> ";
          if ($var1 >= $var2) {
               echo "$var1 <span style='color:red;'>$var2</span>";
          } else {
               echo "$var2 <span style='color:red;'>$var1</span>";
          }
     }
     ?>
</body>
</html>