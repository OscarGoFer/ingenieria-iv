<!DOCTYPE html>
<html lang="es">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 8</title>
</head>

<body>
     <form method="POST">
          <label for="N">N</label>
          <input type="number" name="N" id="N">

          <label for="M">M</label>
          <input type="number" name="M" id="M">

          <button type="submit" name="crearMatriz">Crear matriz</button>
     </form>
     <?php
     if (isset($_POST['crearMatriz'])) {
          if (!empty($_POST['N']) && !empty($_POST['M'])) {
               crearMatriz($_POST['N'], $_POST['M']);
          }
     }
     $matriz = [];
     function crearMatriz($N, $M)
     {
          echo "<br><pre>";
          for ($i = 0; $i < $N; $i++) {
               for ($b = 0; $b < $M; $b++) {
                    $matriz[$i][$b] = rand(0, 100);
                    echo "\t" . $matriz[$i][$b];
               }
               echo "\n\n";
          }
          echo "</pre>";
     }

     ?>
</body>

</html>