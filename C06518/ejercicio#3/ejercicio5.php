<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 5</title>
</head>
<body>
     <?php
     $form = <<<EOD
          <form method="GET">
               <label for="nombre">Nombre:</label>
               <br>
               <input type="text" name="nombre" id="nombre">
               <br><br>

               <label for="apellido">Apellido:</label>
               <br>
               <input type="text" name="apellido" id="apellido">
               <br><br>

               <label for="edad">Edad:</label>
               <br>
               <input type="text" name="edad" id="edad">
               <br><br>

               <button type="submit" id="nombre">Enviar</button>
          </form>
     EOD;
     echo $form;
     ?>
</body>
</html>