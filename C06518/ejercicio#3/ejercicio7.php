<!DOCTYPE html>
<html lang="es">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 7</title>
</head>

<body>
     <?php
     function polindromo($palabra)
     {
          if (strlen($palabra) < 2) {
               return false;
          }
          $palabra = strtolower(str_replace([" ", ",", "."], "", $palabra));

          for ($i = 0; $i < strlen($palabra); $i++) {
               if ($palabra[$i] != $palabra[strlen($palabra) - $i - 1]) {
                    return false;
               }
          }
          return true;
     }
     $palabra = 'radar';
     if (polindromo($palabra)) {
          echo "La palabra '$palabra' es políndromo.";
     } else {
          echo "La palabra '$palabra' no es políndromo.";
     }
     ?>
</body>

</html>