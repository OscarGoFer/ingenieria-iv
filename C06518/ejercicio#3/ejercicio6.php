<!DOCTYPE html>
<html lang="es">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 6</title>
</head>

<body>
     <form method='POST'>
          <label for='numero1'>Número 1:</label>
          <input type='number' name='numero1'>
          <br><br>
          <label for='numero2'>Número 2:</label>
          <input type='number' name='numero2'>
          <br><br>
          <label for='select'>Seleccionar operación:</label>
          <select name='select'>
               <option>sumar</option>
               <option>restar</option>
               <option>multiplicar</option>
               <option>dividir</option>
          </select>
          <br><br>
          <button type='submit' name="enviar">Calcular</button>
     </form>
     <?php
     if (isset($_POST['enviar']) && isset($_POST['numero1']) && isset($_POST['numero2']) && isset($_POST['select'])) {
          if (!empty($_POST['numero1']) && !empty($_POST['numero2'])) {
               $numero1 = $_POST['numero1'];
               $numero2 = $_POST['numero2'];
               $operador = $_POST['select'];
               $resultado;
               switch ($operador) {
                    case 'sumar':
                         $resultado = $numero1 + $numero2;
                         break;
                    case 'restar':
                         $resultado = $numero1 - $numero2;
                         break;
                    case 'multiplicar':
                         $resultado = $numero1 * $numero2;
                         break;
                    case 'dividir':
                         $resultado = $numero1 / $numero2;
                         break;
               }
               echo "
                    <br>
                    resultado: $resultado
               ";
          } else {
               echo '<br>Introduzca todos los datos para realizar la operación.';
          }
     }
     ?>
</body>

</html>