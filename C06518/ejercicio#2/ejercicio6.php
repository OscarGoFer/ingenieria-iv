<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 6</title>
</head>
<body>
     <?php
     $a = rand(99, 999);
     $b = rand(99, 999);
     $c = rand(99, 999);
     echo '$a = ' . $a . '<br>';
     echo '$b = ' . $b . '<br>';
     echo '$c = ' . $c . '<br>';
     echo '$a * 3 = ' . $a * 3 . '<br>';
     echo '$b + $c = ' . $b + $c . '<br>';
     $result = $a * 3 > $b + $c ? '$a * 3 es mayor que $b + $c': '$b + $c es mayor o igual que $a * 3';
     echo $result;
     ?>
</body>
</html>