<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 5</title>
</head>
<body>
     <table>
          <?php
          $table = [9, 18, 27, 36, 45, 54, 63, 72, 81, 90];
          foreach ($table as $number) {
               if ($number%2 == 0){
                    echo
                         "<tr>
                              <td style='background-color: grey; border: 1px solid; padding: 5px; text-align: center;'>$number</td>
                         </tr>";
               } else {
                    echo
                         "<tr>
                              <td style='border: 1px solid; padding: 5px; text-align: center;'>$number</td>
                         </tr>";
               }
          } 
          ?>
     </table>
</body>
</html>