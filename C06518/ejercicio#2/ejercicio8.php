<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 8</title>
</head>
<body>
     <?php
     $numbers = [];
     $band = true;
     while($band){
          $rand_number = rand(1, 10000);
          if ($rand_number%2 == 0) {
               array_unshift($numbers, $rand_number);
          }
          if (sizeof($numbers) == 900) {
               $band = false;
          }
     }
     echo 'cantidad de números: ' . sizeof($numbers) . '<br>números:<br>';
     foreach ($numbers as $number) {
          echo $number . '<br>';
     }
     ?>
</body>
</html>