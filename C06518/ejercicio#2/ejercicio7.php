<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Ejercicio 7</title>
</head>
<body>
     <?php
     $parcial1 = rand(0, 30);
     $parcial2 = rand(0, 20);
     $final1 = rand(0, 50);
     $acumulado = $parcial1 + $parcial2 + $final1;
     echo '$parcial1 = ' . $parcial1 . '<br>';
     echo '$parcial2 = ' . $parcial2 . '<br>';
     echo '$final1 = ' . $final1 . '<br>';
     echo '$acumulado = ' . $acumulado . '<br>';
     switch (true) {
          case ($acumulado < 60):
               echo 'nota: ' . 1;
               break;
          case ($acumulado < 70):
               echo 'nota: ' . 2;
               break;
          case ($acumulado < 80):
               echo 'nota: ' . 3;
               break;
          case ($acumulado < 90):
               echo 'nota: ' . 4;
               break;
          default:
               echo 'nota: ' . 5;
               break;
     }
     ?>
</body>
</html>